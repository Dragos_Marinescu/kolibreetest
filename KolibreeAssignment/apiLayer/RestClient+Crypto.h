//
//  RestClient+Crypto.h
//  KolibreeAssignment
//
//  Created by Oshel on 2/10/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import "RestClient.h"

@interface RestClient (Crypto)
+(NSString*)createSignatureForEndpoint:(NSString*)absoluteURL;

@end

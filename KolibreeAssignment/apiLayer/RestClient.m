//
//  RestClient.m
//  KolibreeTest
//
//  Created by Oshel on 2/9/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import "RestClient.h"
#import "RestClient+Crypto.h"
#import "Constants.h"
#import "TokenManager.h"


@implementation RestClient

+(RestClient*)sharedInstance
{
    static RestClient* client = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[RestClient alloc]init];
    });
    
    return client;
}

-(id)init
{
    if (self = [super init]) {
        [self setupSession];
    }
    return self;
}

-(void)setupSession
{
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    _sessionManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:config];
    
    _serializer = [AFJSONRequestSerializer serializer];
    _sessionManager.requestSerializer = _serializer;
    
    [_sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [_sessionManager.requestSerializer setValue:CLIENT_ID forHTTPHeaderField:CLIENT_ID_HEADER];
}


-(void)startPostRequestWithParams:(NSDictionary*)params urlString:(NSString*)urlStr dataType:(DataType)type
{
    [_sessionManager.requestSerializer setValue:[RestClient createSignatureForEndpoint:urlStr] forHTTPHeaderField:SIG_HEADER];
    
    [_sessionManager POST:urlStr
               parameters:params
                 progress:nil
                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                      NSLog(@"Data %@",responseObject);
                      if ([_dataDelegate respondsToSelector:@selector(didReceiveData:type:)]) {
                          [_dataDelegate didReceiveData:responseObject type:type];
                      }
                      
                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                      
                      NSLog(@"ERRROOOOOR: %@",error.description);
                  }];
    
}


-(void)loginUserWithEmail:(NSString*)email andPassword:(NSString*)password
{
    NSDictionary* params = @{@"email" : email, @"password" : password};
    
    [self startPostRequestWithParams:params urlString:ACCOUNT_URL dataType:TypeLogin];
   
}


-(void)addNewProfile:(UserProfile*)profile
{
    NSDictionary* params = @{@"first_name" : profile.firstName, @"last_name" : profile.lastName, @"gender": profile.gender,@"survey_handedness" :profile.handedness,@"birthday":profile.birthday};
    NSString* url = [NSString stringWithFormat:ADD_PROFILE_URL,[TokenManager getAccountID]];
    NSString* token = [TokenManager getTokenOfType:TokenRequest];
    [_sessionManager.requestSerializer setValue:token forHTTPHeaderField:TOKEN_HEADER];
    
     [self startPostRequestWithParams:params urlString:url dataType:TypeAddProfile];
}


@end

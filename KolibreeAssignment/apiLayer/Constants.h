//
//  Constants.h
//  KolibreeAssignment
//
//  Created by Oshel on 2/12/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

//#define BASE_URL              @"https://test.api.kolibree.com"
//#define ACCOUNT_URL           BASE_URL @"/v1/accounts/request_token/"
//#define client_secret @"X7doOhLCRbuT0FIgBsmy"

#ifdef DEBUG
#define BASE_URL @"https://test.api.kolibree.com"
#else
#define BASE_URL @"http://myproductionserver.com/"
#endif

#define ACCOUNT_URL           BASE_URL @"/v1/accounts/request_token/"
#define ADD_PROFILE_URL       BASE_URL @"/v1/accounts/%@/profiles/"


#define CLIENT_SECRET                  @"X7doOhLCRbuT0FIgBsmy"
#define CLIENT_ID                      @"5"

#define CLIENT_ID_HEADER               @"http-x-client-id"
#define SIG_HEADER                     @"http-x-client-sig"
#define TOKEN_HEADER                   @"http-x-access-token"

#endif /* Constants_h */

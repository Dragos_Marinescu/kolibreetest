//
//  Parser.m
//  KolibreeAssignment
//
//  Created by Oshel on 2/10/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import "Parser.h"
#import "TokenManager.h"
#import "UserProfile.h"

@implementation Parser

+(Parser*)sharedInstance
{
    static Parser* parser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        parser = [[Parser alloc]init];
    });
    
    return parser;
}



-(UserProfile*)parseUserProfile:(NSDictionary*)dict
{
    NSParameterAssert(dict);
    
    UserProfile* profile = [UserProfile new];
    
    @try {
        profile.firstName = [dict objectForKey:@"first_name"];
        profile.lastName  = [dict objectForKey:@"last_name"];
        profile.gender    = [dict objectForKey:@"gender"];
    } @catch (NSException *exception) {
        NSLog(@"Exception on parsing profile data %@",exception.description);
    }
    
    return profile;
    
}


-(void)didReceiveData:(id)data type:(DataType)type
{
    NSMutableArray* collection = [NSMutableArray new];
    
    if (type == TypeLogin)
    {
        [TokenManager saveToken:[data objectForKey:@"access_token"]  ofType:TokenRequest];
        [TokenManager saveToken:[data objectForKey:@"refresh_token"] ofType:TokenRefresh];
        [TokenManager saveAccountID:[data objectForKey:@"id"]];
        
        NSArray* profilesArr = [data objectForKey:@"profiles"];
        for (NSDictionary* profile in profilesArr) {
            UserProfile* up = [self parseUserProfile:profile];
            [collection addObject:up];
        }
        
        if ([_delegate respondsToSelector:@selector(didReceiveUserProfiles:)]) {
            [_delegate didReceiveUserProfiles:collection];
        }
    }
    if (type == TypeAddProfile) {
        
    }
}



@end

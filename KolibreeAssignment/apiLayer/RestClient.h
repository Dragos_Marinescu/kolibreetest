//
//  RestClient.h
//  KolibreeTest
//
//  Created by Oshel on 2/9/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "UserProfile.h"

@class Parser;

typedef enum : NSUInteger {
    TypeLogin,
    TypeAddProfile,
} DataType;

@protocol KBDataDelegate <NSObject>
@required
- (void)didReceiveData:(id)data type:(DataType)type;

@end

@interface RestClient : NSObject
@property(nonatomic,strong) Parser* parser;

+ (RestClient*)sharedInstance;
- (void)loginUserWithEmail:(NSString*)email andPassword:(NSString*)password;
-(void)addNewProfile:(UserProfile*)profile;

@property(nonatomic,strong) AFHTTPSessionManager* sessionManager;
@property(nonatomic,strong) AFJSONRequestSerializer* serializer;

@property(nonatomic,strong) id <KBDataDelegate> dataDelegate;

@end

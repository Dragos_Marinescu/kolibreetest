//
//  TokenManager.m
//  KolibreeAssignment
//
//  Created by Oshel on 2/12/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import "TokenManager.h"

#define REFRESH_TOKEN @"refresh" // used to fetch new access_token
#define REQUEST_TOKEN @"request"

#define ACCOUNT_KEY   @"account"

@implementation TokenManager
+(void)saveToken:(NSString *)token ofType:(TokenType)tokenType
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* key = tokenType ? REFRESH_TOKEN:REQUEST_TOKEN;
    
    [defaults setObject:token forKey:key];
    [defaults synchronize];
    
}

+(NSString*)getTokenOfType:(TokenType)tokenType
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* key = tokenType ? REFRESH_TOKEN:REQUEST_TOKEN;
    
    return [defaults objectForKey:key];

}

+(void)saveAccountID:(NSString *)iD
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:iD forKey:ACCOUNT_KEY];
    [defaults synchronize];
    
}

+(NSString*)getAccountID
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:ACCOUNT_KEY];
}

@end

//
//  Parser.h
//  KolibreeAssignment
//
//  Created by Oshel on 2/10/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestClient.h"

@protocol ParserDelegate <NSObject>

-(void)didReceiveUserProfiles:(NSArray*)profiles;

@end

@interface Parser : NSObject <KBDataDelegate>
+(Parser*)sharedInstance;

@property(nonatomic,strong) id <ParserDelegate> delegate;
@end

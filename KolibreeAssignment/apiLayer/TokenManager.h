//
//  TokenManager.h
//  KolibreeAssignment
//
//  Created by Oshel on 2/12/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum : NSUInteger {
    TokenRequest = 0,
    TokenRefresh,
} TokenType;

@interface TokenManager : NSObject

+(void)saveToken:(NSString*)token ofType:(TokenType)tokenType;
+(NSString*)getTokenOfType:(TokenType)tokenType;


+(void)saveAccountID:(NSString*)iD;
+(NSString*)getAccountID;


@end

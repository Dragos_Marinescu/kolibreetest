//
//  ProfileCell.m
//  KolibreeAssignment
//
//  Created by Oshel on 2/13/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import "ProfileCell.h"

@implementation ProfileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

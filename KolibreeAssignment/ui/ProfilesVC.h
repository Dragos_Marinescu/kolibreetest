//
//  ProfilesVC.h
//  KolibreeAssignment
//
//  Created by Oshel on 2/12/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfilesVC : UIViewController
@property(nonatomic,strong) NSArray* profilesArray;
@end

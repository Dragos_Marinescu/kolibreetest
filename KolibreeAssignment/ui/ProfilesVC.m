//
//  ProfilesVC.m
//  KolibreeAssignment
//
//  Created by Oshel on 2/12/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import "ProfilesVC.h"
#import "UIView+YGPulseView.h"
#import "RestClient.h"
#import "ProfileCell.h"
#import "KolibreeAssignment-Swift.h"
#import "DateUtils.h"

@interface ProfilesVC () <UITableViewDelegate,UITableViewDataSource>
{
    UIDatePicker* datePicker;
    UserProfile* profile;
}
@property (strong, nonatomic) IBOutlet UIView *addProfileButton;
@property (strong, nonatomic) IBOutlet UITableView *profilesTableView;

///profile outlets
@property (strong, nonatomic) IBOutlet AMInputView *firstNameView;
@property (strong, nonatomic) IBOutlet AMInputView *lastNameView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *genderControl;
@property (strong, nonatomic) IBOutlet UISegmentedControl *handControl;
@property (strong, nonatomic) IBOutlet AMInputView *birthdayView;

@end

@implementation ProfilesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [self setupPickerView];
    
    //[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(orientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initProfile];

}


-(void)initProfile
{
    profile = [UserProfile new];
    profile.handedness = @"R"; //default
    profile.gender = @"M"; //default
}


#pragma mark === TableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _profilesArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileCell* cell = [tableView dequeueReusableCellWithIdentifier:@"profileCell"];
    UserProfile* p = _profilesArray[indexPath.row];
    
    cell.nameLabel.text= [NSString stringWithFormat:@"%@ %@",p.firstName,p.lastName];
    cell.genderLabel.text = p.gender;
    
    return cell;
}

#pragma mark ===
-(void)setupView
{
    self.navigationController.navigationBarHidden = NO;
    _addProfileButton.layer.cornerRadius = 30.0f;
    _addProfileButton.center = CGPointMake(self.view.frame.size.width/2, _addProfileButton.frame.origin.y); //quick fix for some screens.
    [_addProfileButton startPulseWithColor:_addProfileButton.backgroundColor animation:YGPulseViewAnimationTypeRadarPulsing];
}

-(void)setupPickerView
{
    datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode  = UIDatePickerModeDate;
    datePicker.backgroundColor = [UIColor whiteColor];
    [datePicker setValue:[UIColor blackColor] forKeyPath:@"textColor"];
    
    [datePicker addTarget:self action:@selector(dateDidChange:) forControlEvents:UIControlEventValueChanged];
    _birthdayView.textFieldView.inputView = datePicker;
}

-(void)dateDidChange:(UIDatePicker*)sender
{
    _birthdayView.textFieldView.text = [DateUtils stringFromDatePrettyPrinted:datePicker.date];
}
- (IBAction)gendePressed:(UISegmentedControl *)sender {
    profile.gender = [[sender titleForSegmentAtIndex:sender.selectedSegmentIndex] substringToIndex:1];
}

- (IBAction)handPressed:(UISegmentedControl *)sender {
    profile.handedness = [[sender titleForSegmentAtIndex:sender.selectedSegmentIndex] substringToIndex:1];
}

-(void)collectData
{
    profile.firstName = _firstNameView.textFieldView.text;
    profile.lastName  = _lastNameView.textFieldView.text;
    profile.birthday  = _birthdayView.textFieldView.text;
}


-(BOOL)fieldsEmpty
{
    return _firstNameView.textFieldView.text.length == 0 ||
           _lastNameView.textFieldView.text.length  == 0 ||
           _birthdayView.textFieldView.text.length  == 0;
}



- (IBAction)dismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)addProfileButtonPressed:(id)sender {
    
    if (![self fieldsEmpty]) {
        [self collectData];
        [[RestClient sharedInstance] addNewProfile:profile];
    }
    else {
        //inform user
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

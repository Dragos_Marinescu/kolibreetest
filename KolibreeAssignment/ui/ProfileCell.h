//
//  ProfileCell.h
//  KolibreeAssignment
//
//  Created by Oshel on 2/13/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *genderLabel;

@end

//
//  ViewController.m
//  KolibreeAssignment
//
//  Created by Oshel on 2/10/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import "ViewController.h"
#import "RestClient.h"
#import "KolibreeAssignment-Swift.h"
#import "Parser.h"
#import "ProfilesVC.h"

@interface ViewController () <ParserDelegate>
{
    NSArray* userProfiles;
}

@property (strong, nonatomic) IBOutlet AMInputView *emailView;
@property (strong, nonatomic) IBOutlet AMInputView *passwordView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Parser sharedInstance].delegate = self;
    
    //_emailView.textFieldView.text    = @"kolibree.test02@test.com";
    //_passwordView.textFieldView.text = @"4yage";
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)fieldsEmpty
{
    return _emailView.textFieldView.text.length == 0 ||
           _passwordView.textFieldView.text.length == 0;
}


- (IBAction)didPressLogin:(id)sender
{
    if (![self fieldsEmpty]) {
        NSString* email = _emailView.textFieldView.text;
        NSString* password = _passwordView.textFieldView.text;
        
        [[RestClient sharedInstance] loginUserWithEmail:email
                                            andPassword:password];
        _activityIndicator.hidden = NO;
        _loginButton.enabled = NO;
    }
    else {
        //show alert to user
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqual:@"profileSegue"]) {
        ProfilesVC* vc = segue.destinationViewController;
        vc.profilesArray = [NSMutableArray new];
        vc.profilesArray = userProfiles;
    }
}


-(void)didReceiveUserProfiles:(NSArray *)profiles
{
    userProfiles = profiles;
    _activityIndicator.hidden = YES;
    _loginButton.enabled = YES;
    
    [self performSegueWithIdentifier:@"profileSegue" sender:self];
}

@end

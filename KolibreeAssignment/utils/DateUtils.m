//
//  DateUtils.m
//  KolibreeAssignment
//
//  Created by Oshel on 2/13/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils
+(NSString*)stringFromDatePrettyPrinted:(NSDate*)date
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    return [dateFormatter stringFromDate:date];
}
@end

//
//  DateUtils.h
//  KolibreeAssignment
//
//  Created by Oshel on 2/13/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtils : NSObject
+(NSString*)stringFromDatePrettyPrinted:(NSDate*)date;

@end

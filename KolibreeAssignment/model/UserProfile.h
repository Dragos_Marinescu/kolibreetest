//
//  UserProfile.h
//  KolibreeTest
//
//  Created by Oshel on 2/10/17.
//  Copyright © 2017 Kolibree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserProfile : NSObject

@property(nonatomic,strong) NSString *firstName,
                                     *lastName,
                                     *gender,
                                     *birthday,
                                     *handedness;
@end
